import React, { useReducer, useMemo, useContext, useCallback } from "react";
import { CacheService } from "./cache.service";

interface IOnboardingState {
  isOnboardingDone: boolean;
}

//TODO: register
type OnboardingAction = { type: "onboardingDone" };

interface IOnboardingContext {
  OnboardingState: IOnboardingState;
  onboardingDone: () => void;
}

const defaultOnboardingContext: IOnboardingContext = {
  OnboardingState: { isOnboardingDone: false },
  onboardingDone: () => {}
};

const OnboardingContext = React.createContext<IOnboardingContext>(defaultOnboardingContext);

const reducer = (state: IOnboardingState, action: OnboardingAction) => {
  switch (action.type) {
    case "onboardingDone": {
      return {
        ...state,
        isOnboardingDone: true
      };
    }
    default:
      throw new Error("No such type for action.type in ToasterAction");
  }
};

function getOnboardingStore() {
  interface IProvider {
    isOnboardingDone?: boolean;
  }
  const Provider: React.FC<IProvider> = ({ children, isOnboardingDone }) => {
    const [state, dispatch] = useReducer(reducer, {
      isOnboardingDone: isOnboardingDone === undefined ? false : isOnboardingDone
    });

    const onboardingDone = useCallback(async () => {
      await CacheService.setOnboardingDone(true);
      dispatch({ type: "onboardingDone" });
    }, []);

    const contextValue = useMemo(() => state, [state]);
    return (
      <OnboardingContext.Provider
        value={{
          OnboardingState: contextValue,
          onboardingDone
        }}
      >
        {children}
      </OnboardingContext.Provider>
    );
  };

  const useStore = () => useContext<IOnboardingContext>(OnboardingContext);

  return { Provider, useStore };
}

const OnboardingService = getOnboardingStore();
export default OnboardingService;
