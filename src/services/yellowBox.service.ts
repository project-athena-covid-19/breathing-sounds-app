import { YellowBox } from "react-native";

export class YellowBoxService {
  static removeYellowBoxes = () => {
    // TODO: remove once expo updates lottie to >3.1.0
    // Issue: https://github.com/react-native-community/lottie-react-native/issues/502
    YellowBox.ignoreWarnings(["ReactNative.NativeModules.LottieAnimationView"]);
    // TODO Loading has the a width prop type of number, we provide e.g. '80%', works bc passed on to svg component
    // Fork maybe?
    YellowBox.ignoreWarnings(["SvgAnimatedLinearGradient"]);
    YellowBox.ignoreWarnings(["AsyncStorage has been extracted"]);
  };
}
