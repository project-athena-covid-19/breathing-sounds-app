import React, { useReducer, useMemo, useContext, useCallback } from "react";
import { CacheService } from "./cache.service";

interface IAuthState {
  isLoggedIn: boolean;
  isLoading: boolean;
}

//TODO: rigster
type AuthAction = { type: "login" } | { type: "logout" } | { type: "finishLogin" } | { type: "finishLogout" };

interface IAuthContext {
  AuthState: IAuthState;
  login: () => void;
  logout: () => void;
}

const defaultAuthContext: IAuthContext = {
  AuthState: { isLoading: false, isLoggedIn: false },
  login: () => {},
  logout: () => {}
};

const AuthContext = React.createContext<IAuthContext>(defaultAuthContext);

const reducer = (state: IAuthState, action: AuthAction) => {
  switch (action.type) {
    case "login": {
      return {
        ...state,
        isLoading: true
      };
    }
    case "finishLogin": {
      return {
        ...state,
        isLoggedIn: true,
        isLoading: false
      };
    }
    case "logout": {
      return {
        ...state,
        isLoading: true
      };
    }
    case "finishLogout": {
      return {
        ...state,
        isLoggedIn: false,
        isLoading: false
      };
    }
    default:
      throw new Error("No such type for action.type in ToasterAction");
  }
};

function getAuthStore() {
  interface IProvider {
    cachedToken?: string;
  }
  const Provider: React.FC<IProvider> = ({ children, cachedToken }) => {
    const [state, dispatch] = useReducer(reducer, {
      isLoggedIn: !!cachedToken,
      isLoading: false
    });

    const login = useCallback(async () => {
      //TODO some login
      dispatch({ type: "login" });
      await CacheService.setAuthToken("testToken");

      //do requests

      dispatch({ type: "finishLogin" });
    }, []);

    const logout = useCallback(async () => {
      //TODO some login
      dispatch({ type: "logout" });
      await CacheService.removeAuthToken();

      //do requests

      dispatch({ type: "finishLogout" });
    }, []);

    const contextValue = useMemo(() => state, [state]);
    return (
      <AuthContext.Provider
        value={{
          AuthState: contextValue,
          login,
          logout
        }}
      >
        {children}
      </AuthContext.Provider>
    );
  };

  const useStore = () => useContext<IAuthContext>(AuthContext);

  return { Provider, useStore };
}

const AuthService = getAuthStore();
export default AuthService;
