import i18n, { LanguageDetectorModule } from "i18next";
import * as Localization from "expo-localization";

import { initReactI18next } from "react-i18next";
import * as lng from "assets/translations";

let resources = {
  en: {},
  de: {}
};

for (const namespace in lng) {
  // @ts-ignore
  if (lng[namespace].en) {
    // @ts-ignore
    resources.en[namespace] = lng[namespace].en;
  }
  // @ts-ignore
  if (lng[namespace].de) {
    // @ts-ignore
    resources.de[namespace] = lng[namespace].de;
  }
}

const LanguageDetector: LanguageDetectorModule = {
  type: "languageDetector",
  detect: () => Localization.locale,
  init: () => {},
  cacheUserLanguage: () => {}
};

i18n
  .use(LanguageDetector)
  .use(initReactI18next)
  .init({
    fallbackLng: "en",
    resources: resources,
    interpolation: {
      escapeValue: false // not needed for react as it escapes by default
    }
  });

export default i18n;
