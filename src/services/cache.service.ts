import { AsyncStorage } from "react-native";
import * as SecureStore from "expo-secure-store";

import { Theme } from "./theme.service";

const THEME_KEY: string = "themeKey";
const LANGUAGE_KEY: string = "languageCodeKey";
const ONBOARDING_KEY: string = "onboardingStateKey";
const AUTH_TOKEN = "token";
const SLUG: string = "@myAppCache:";
const SLUG_SECURE: string = "secure";

export class CacheService {
  static getThemeKey = async (fallback?: Theme): Promise<Theme> => {
    return AsyncStorage.getItem(SLUG + THEME_KEY).then((theme) => {
      return theme ? JSON.parse(theme) : fallback;
    });
  };

  static setThemeKey = (theme: Theme): Promise<void> => {
    return AsyncStorage.setItem(SLUG + THEME_KEY, JSON.stringify(theme));
  };

  static getLanguageCode = async (fallback?: string): Promise<string> => {
    return AsyncStorage.getItem(SLUG + LANGUAGE_KEY).then((languageCode) => {
      return languageCode ? JSON.parse(languageCode) : fallback;
    });
  };

  static setLanguageCode = (languageCode: string): Promise<void> => {
    return AsyncStorage.setItem(SLUG + LANGUAGE_KEY, JSON.stringify(languageCode));
  };

  static getOnboardingState = async (fallback?: boolean): Promise<boolean> => {
    return AsyncStorage.getItem(SLUG + ONBOARDING_KEY).then((onboardingDone) => {
      console.log("get", onboardingDone);
      return onboardingDone ? JSON.parse(onboardingDone) : fallback;
    });
  };

  /**
   * @param onboardingDone - true = onboarding done
   *                       - false = reset onboarding state (For testing only!!!)
   */
  static setOnboardingDone = async (onboardingDone: boolean): Promise<void> => {
    const test = await AsyncStorage.setItem(SLUG + ONBOARDING_KEY, JSON.stringify(onboardingDone));
    console.log("set", test);
    return test;
    return AsyncStorage.setItem(SLUG + ONBOARDING_KEY, JSON.stringify(onboardingDone));
  };

  static getAuthToken = (): Promise<string | undefined> => {
    return SecureStore.getItemAsync(SLUG_SECURE + AUTH_TOKEN).then((token) => {
      return token ? JSON.parse(token) : undefined;
    });
    // return AsyncStorage.getItem(SLUG + AUTH_TOKEN).then((token) => {
    //   return token ? JSON.parse(token) : undefined;
    // });
  };

  static setAuthToken = (token: string): Promise<void> => {
    return SecureStore.setItemAsync(SLUG_SECURE + AUTH_TOKEN, JSON.stringify(token));
    // return AsyncStorage.setItem(SLUG + AUTH_TOKEN, JSON.stringify(token));
  };

  static removeAuthToken = (): Promise<void> => {
    return SecureStore.deleteItemAsync(SLUG_SECURE + AUTH_TOKEN);
    // return AsyncStorage.removeItem(SLUG + AUTH_TOKEN);
  };
}
