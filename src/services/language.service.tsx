import * as Localization from "expo-localization";
import React, { useReducer, useMemo, useContext, useCallback, useEffect } from "react";
import { CacheService } from "./cache.service";
import { useTranslation } from "react-i18next";
import moment from "moment";

interface ILanguageState {
  activeLanguageCode: string;
}

type LanguageAction = { type: "set"; languageCode: string };

interface ILanguageContext {
  languageState: ILanguageState;
  setLanguage: (languageCode: string) => void;
}

const LanguageContext = React.createContext<ILanguageContext>(null);

function getLanguageStore() {
  const reducer = (state: ILanguageState, action: LanguageAction) => {
    switch (action.type) {
      case "set": {
        return {
          ...state,
          activeLanguageCode: action.languageCode
        };
      }
      default:
        throw new Error("No such type for action.type in ToasterAction");
    }
  };

  interface IProvider {
    activeLanguageCode?: string;
  }
  const Provider: React.FC<IProvider> = ({ children, activeLanguageCode }) => {
    const { i18n } = useTranslation();
    const code = activeLanguageCode || Localization.locale;
    useEffect(() => {
      moment.locale(code);
      i18n.changeLanguage(code);
    }, []);

    const [state, dispatch] = useReducer(reducer, {
      activeLanguageCode: code
    });

    const contextValue = useMemo(() => state, [state]);
    return (
      <LanguageContext.Provider
        value={{
          languageState: contextValue,
          setLanguage: useCallback(async (languageCode: string) => {
            await CacheService.setLanguageCode(languageCode);
            moment.locale(languageCode);
            i18n.changeLanguage(languageCode);
            dispatch({ type: "set", languageCode });
          }, [])
        }}
      >
        {children}
      </LanguageContext.Provider>
    );
  };

  const useStore = () => useContext<ILanguageContext>(LanguageContext);

  return { Provider, useStore };
}

const LanguageService = getLanguageStore();
export default LanguageService;
