import React from "react";
import { DefaultTheme, NavigationState } from "@react-navigation/native";
import { useTheme } from "@ui-kitten/components";
import { StackNavigationOptions } from "@react-navigation/stack";
import LinearGradient from "src/components/linearGradient/LinearGradient";

export const NavigatorTheme = {
  ...DefaultTheme,
  colors: {
    ...DefaultTheme.colors,
    // prevent layout blinking when performing navigation
    background: "transparent"
  }
};

export const useDefaultStackNavigatorHeaderStyle = (): StackNavigationOptions => {
  const theme = useTheme();

  return {
    headerTintColor: theme["text-primary-color"],
    // @ts-ignore
    headerTitleStyle: {
      fontWeight: theme["text-subtitle-1-font-weight"],
      // fontSize: theme["text-subtitle-1-font-size"],
      lineHeight: theme["text-subtitle-1-line-height"]
    },
    headerStyle: {
      backgroundColor: theme["background-basic-color-1"],
      elevation: 0,
      borderBottomWidth: 0
    },
    // @ts-ignore
    headerBackTitleStyle: {
      fontWeight: theme["text-subtitle-1-font-weight"],
      // fontSize: theme["text-subtitle-1-font-size"],
      lineHeight: theme["text-subtitle-1-line-height"],
      color: theme["text-primary-color"]
    }
  };
};

export const useTransparentHeaderStyle = (): StackNavigationOptions => {
  return {
    headerTintColor: "white",
    headerTransparent: true
  };
};

export const useLinearGradiantStyle = (): StackNavigationOptions => {
  return {
    headerTintColor: "white",
    headerBackground: () => <LinearGradient />
  };
};

export const getActiveRouteName = (navigationState: NavigationState | undefined): string => {
  if (!navigationState) {
    return "";
  }
  const route = navigationState.routes[navigationState.index];
  // dive into nested navigators
  //@ts-ignore
  if (route.state) {
    //@ts-ignore
    return getActiveRouteName(route.state);
  }
  //@ts-ignore
  return route.name;
};
