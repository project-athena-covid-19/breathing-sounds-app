import { AppRoute } from "./routes";

const NavigationConfig = {
  [AppRoute.MAIN_STACK]: {
    path: "mainStack",
    screens: {
      [AppRoute.HOME_STACK]: {
        path: "homeStack",
        screens: {
          [AppRoute.HOME]: "home"
        }
      },
      [AppRoute.ACCOUNT_STACK]: {
        path: "accountStack",
        screens: {
          [AppRoute.ACCOUNT]: "account"
        }
      }
    }
  }
};

export default NavigationConfig;
