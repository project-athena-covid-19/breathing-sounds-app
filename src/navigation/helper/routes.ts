export enum AppRoute {
  ONBOARDING = "Onboarding",
  MAIN_STACK = "MainStack",
  HOME_STACK = "HomeStack",
  HOME = "Home"
}
