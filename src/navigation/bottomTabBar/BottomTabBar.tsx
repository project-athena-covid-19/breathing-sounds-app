import React from "react";
import { IconElement, BottomNavigationTab, Divider, BottomNavigation } from "@ui-kitten/components";
import { BottomTabBarProps } from "@react-navigation/bottom-tabs";

import { ImageStyle } from "react-native";
import { SafeAreaLayout } from "../../components/safeAreaLayout/SafeAreaLayout";
import { Icon } from "src/components";

const HomeIcon = (style: ImageStyle): IconElement => <Icon tintColor={style.tintColor} name="home-outline" />;

const BottomTabBar = (props: BottomTabBarProps): React.ReactElement => {
  const onSelect = (index: number): void => {
    switch (index) {
      case 0:
        props.navigation.navigate(props.state.routeNames[0]);
        break;
      default:
        break;
    }
  };

  const getSelectedIndex = (index: number) => {
    switch (index) {
      case 1:
        return 1;
      default:
        index;
    }
  };

  return (
    <SafeAreaLayout insets="bottom">
      <Divider />
      <BottomNavigation
        appearance="noIndicator"
        selectedIndex={getSelectedIndex(props.state.index)}
        onSelect={onSelect}
      >
        <BottomNavigationTab icon={HomeIcon} />
      </BottomNavigation>
    </SafeAreaLayout>
  );
};

export default BottomTabBar;
