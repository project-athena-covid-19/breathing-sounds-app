import React from "react";
import { RouteProp } from "@react-navigation/native";
import { StackNavigationProp, createStackNavigator } from "@react-navigation/stack";
import { AppRoute } from "src/navigation/helper/routes";
import Onboarding from "src/screens/auth/onboarding/OnboardingScreen.component";
import { useDefaultStackNavigatorHeaderStyle } from "src/navigation/helper";

/********************************************** types ********************************************* */
type AuthStackParamList = {
  [AppRoute.ONBOARDING]: undefined;
};
export interface IOnboardingScreenContainerProps {
  navigation: StackNavigationProp<AuthStackParamList, AppRoute.ONBOARDING>;
  route: RouteProp<AuthStackParamList, AppRoute.ONBOARDING>;
}
/********************************************** Navigator ********************************************* */
const Stack = createStackNavigator<AuthStackParamList>();
interface IAuthStackNavigatorProps {}
const AuthStackNavigator: React.FC<IAuthStackNavigatorProps> = () => {
  return (
    <Stack.Navigator
      initialRouteName={AppRoute.ONBOARDING}
      screenOptions={{
        ...useDefaultStackNavigatorHeaderStyle()
      }}
    >
      <Stack.Screen
        name={AppRoute.ONBOARDING}
        component={Onboarding}
        options={{
          header: () => null
        }}
      />
    </Stack.Navigator>
  );
};

export default AuthStackNavigator;
