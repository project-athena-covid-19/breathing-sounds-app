import React, { useEffect, useState } from "react";
import { NavigationContainer, useLinking, NavigationContainerRef } from "@react-navigation/native";

import { enableScreens } from "react-native-screens";

import { Linking } from "expo";
import { NavigatorTheme, getActiveRouteName } from "../helper";
import NavigationConfig from "../helper/config";
import AuthStackNavigator from "./auth/AuthStackNavigator";
import RootStackNavigator from "./main/RootStackNavigator";
import useRenderCount from "src/hooks/useRenderCount";
import OnboardingService from "src/services/onboarding.service";
enableScreens();

const prefix = Linking.makeUrl("/");

const AppNavigator: React.FC = () => {
  useRenderCount("AppNavigator");

  const refNavigationContainer = React.useRef<NavigationContainerRef>();
  const routeNameRef = React.useRef<string>("");
  const { getInitialState } = useLinking(refNavigationContainer, {
    prefixes: [prefix],
    config: NavigationConfig
  });

  const { OnboardingState } = OnboardingService.useStore();

  console.log("OnboardingState", OnboardingState);

  // TODO handle it better with context?
  const [initialState, setInitialState] = useState();
  const [isReady, setIsReady] = useState(false);

  useEffect(() => {
    if (refNavigationContainer.current) {
      const state = refNavigationContainer.current.getRootState();

      // Save the initial route name
      routeNameRef.current = getActiveRouteName(state);
    }
  }, []);

  useEffect(() => {
    Promise.race([
      getInitialState(),
      new Promise((resolve) =>
        // Timeout in 150ms if `getInitialState` doesn't resolve
        // Workaround for https://github.com/facebook/react-native/issues/25675
        setTimeout(resolve, 150)
      )
    ])
      .catch((e) => {
        console.error(e);
      })
      .then((state) => {
        if (state !== undefined) {
          setInitialState(state);
        }

        setIsReady(true);
      });
  }, [getInitialState]);

  if (!isReady) {
    return null;
  }

  return (
    <NavigationContainer
      initialState={initialState}
      ref={refNavigationContainer}
      theme={NavigatorTheme}
      onStateChange={(state) => {
        const previousRouteName = routeNameRef.current;
        const currentRouteName = getActiveRouteName(state);

        if (previousRouteName !== currentRouteName) {
          // The line below uses the @react-native-firebase/analytics tracker
          // Change this line to use another Mobile analytics SDK
        }

        // Save the current route name for later comparision
        routeNameRef.current = currentRouteName;
      }}
    >
      {OnboardingState.isOnboardingDone ? <RootStackNavigator /> : <AuthStackNavigator />}
    </NavigationContainer>
  );
};

export default AppNavigator;
