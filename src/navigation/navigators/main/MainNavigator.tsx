import React from "react";
import { AppRoute } from "../../helper/routes";
import { createBottomTabNavigator, BottomTabBarProps } from "@react-navigation/bottom-tabs";
import HomeStackNavigator from "./HomeNavigator";
import BottomTabBar from "src/navigation/bottomTabBar/BottomTabBar";

type MainTabNavigatorParamList = {
  [AppRoute.ACCOUNT_STACK]: undefined;
  [AppRoute.HOME_STACK]: undefined;
};

const Tab = createBottomTabNavigator<MainTabNavigatorParamList>();

const MainTabNavigator: React.FC = () => {
  return (
    <Tab.Navigator
      initialRouteName={AppRoute.HOME_STACK}
      tabBar={(props: BottomTabBarProps) => <BottomTabBar {...props} />}
    >
      <Tab.Screen name={AppRoute.HOME_STACK} component={HomeStackNavigator} />
    </Tab.Navigator>
  );
};

export default MainTabNavigator;
