import React from "react";
import { createStackNavigator, StackNavigationProp } from "@react-navigation/stack";

import { RouteProp } from "@react-navigation/native";
import { AppRoute } from "src/navigation/helper/routes";
import { useDefaultStackNavigatorHeaderStyle } from "src/navigation/helper";
import HomeScreenContainer from "src/screens/main/home/HomeScreen.container";
import useRenderCount from "src/hooks/useRenderCount";

/********************************************** types ********************************************* */
type HomeStackParamList = {
  [AppRoute.HOME]: undefined;
};

export interface IHomeScreenContainerProps {
  navigation: StackNavigationProp<HomeStackParamList, AppRoute.HOME>;
  route: RouteProp<HomeStackParamList, AppRoute.HOME>;
}

const Stack = createStackNavigator<HomeStackParamList>();

/********************************************** Navigator ********************************************* */

const HomeStackNavigator: React.FC = () => {
  useRenderCount("HomeStackNavigator");

  return (
    <Stack.Navigator
      initialRouteName={AppRoute.HOME}
      screenOptions={{
        ...useDefaultStackNavigatorHeaderStyle()
      }}
    >
      <Stack.Screen name={AppRoute.HOME} component={HomeScreenContainer} />
    </Stack.Navigator>
  );
};

export default HomeStackNavigator;
