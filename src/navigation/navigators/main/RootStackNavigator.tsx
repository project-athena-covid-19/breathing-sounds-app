import React from "react";
import { createStackNavigator } from "@react-navigation/stack";

import { AppRoute } from "../../helper/routes";
import { useDefaultStackNavigatorHeaderStyle } from "../../helper";
import MainStackNavigator from "./MainNavigator";

export type RootStackParamList = {
  [AppRoute.MAIN_STACK]: undefined;
};
const RootStack = createStackNavigator<RootStackParamList>();

const RootStackNavigator: React.FC = () => {
  return (
    <RootStack.Navigator
      mode="modal"
      screenOptions={{
        ...useDefaultStackNavigatorHeaderStyle()
      }}
    >
      <RootStack.Screen name={AppRoute.MAIN_STACK} component={MainStackNavigator} options={{ header: () => null }} />
    </RootStack.Navigator>
  );
};

export default RootStackNavigator;
