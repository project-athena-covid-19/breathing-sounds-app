import React, { useState, useEffect, useRef } from "react";
import { StyleSheet, Dimensions, View, Animated } from "react-native";
// import Image from "react-native-scalable-image";
import Swiper from "react-native-swiper";

import OnboardingService from "src/services/onboarding.service";
import useRenderCount from "src/hooks/useRenderCount";
import LinearGradient from "src/components/linearGradient/LinearGradient";
import { Text, Button, Icon } from "src/components";
import { SafeAreaLayout } from "src/components/safeAreaLayout/SafeAreaLayout";
import { StatusBar } from "src/components/statusBar/StatusBar";
import { IOnboardingScreenContainerProps } from "src/navigation/navigators/auth/AuthStackNavigator";

// const IMAGE_WIDTH = 375;
// const DESIGN_IMAGE_SCREEN_HEIGHT = 812;
// const imageMarginTop =
//   (Dimensions.get("window").width / IMAGE_WIDTH +
//     Math.abs(Dimensions.get("window").height - DESIGN_IMAGE_SCREEN_HEIGHT) / Dimensions.get("window").height -
//     112 / DESIGN_IMAGE_SCREEN_HEIGHT) *
//     100 -
//   100;

const page1Headline = "ONBOARDING INFO 1";
const page1Description = "Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod";
const page2Headline = "ONBOARDING INFO 2";
const page2Description = "Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod";
const page4Headline = "ONBOARDING INFO 3";
const page4Description = "Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod";
const buttonText = "START NOW";

const animationDuration = 500;

interface IProps {}
type OnboardingProps = IProps & IOnboardingScreenContainerProps;

const Onboarding: React.FC<OnboardingProps> = () => {
  useRenderCount("OnboardingScreenComponent");
  const { onboardingDone } = OnboardingService.useStore();
  const [pageIndex, setPageIndex] = useState<number>(0);
  const iconOpacity = useRef(new Animated.Value(1)).current;
  const buttonOpacity = useRef(new Animated.Value(0)).current;

  useEffect(() => {
    if (pageIndex === 2) {
      Animated.timing(iconOpacity, {
        toValue: 0,
        duration: animationDuration
      }).start();
      Animated.timing(buttonOpacity, {
        toValue: 1,
        duration: animationDuration
      }).start();
    } else if (pageIndex === 1) {
      Animated.timing(iconOpacity, {
        toValue: 1,
        duration: animationDuration
      }).start();
      Animated.timing(buttonOpacity, {
        toValue: 0,
        duration: animationDuration
      }).start();
    }
  }, [pageIndex]);

  const handleOnboardingDone = () => {
    onboardingDone();
  };

  const renderContent = (headlineText: string, descriptionText: string, style: any, children?: any) => (
    <View style={{ ...style, ...styles.content }}>
      {children || null}
      <Text category="h4" status="control" style={styles.headline}>
        {headlineText}
      </Text>
      <View style={styles.descriptionContainer}>
        <Text category="p1" status="control" style={styles.description}>
          {descriptionText}
        </Text>
      </View>
    </View>
  );

  const renderPageOneToThree = (source: any, headlineText: string, descriptionText: string) => (
    <LinearGradient>
      <View style={styles.page}>
        {/* <Image width={Dimensions.get("window").width} source={source} style={{ marginTop: `-${imageMarginTop}%` }} /> */}
        {renderContent(headlineText, descriptionText, styles.textContainterPage1To3)}
      </View>
    </LinearGradient>
  );

  const renderFirstPage = () => renderPageOneToThree(null, page1Headline, page1Description);
  const renderSecondPage = () => renderPageOneToThree(null, page2Headline, page2Description);
  // const renderThirdPage = () => renderPageOneToThree(null, page3Headline, page3Description);

  const renderForthPage = () => (
    <LinearGradient>
      <View style={styles.page}>
        {renderContent(
          page4Headline,
          page4Description,
          styles.textContainterPage4
          // <Image height={Dimensions.get("window").height * 0.467} source={page4Image} />
        )}
      </View>
    </LinearGradient>
  );

  return (
    <SafeAreaLayout style={{ flex: 1 }}>
      <StatusBar barStyle="light-content" />
      <Swiper
        loop={false}
        showsButtons={false}
        index={pageIndex}
        onIndexChanged={(idx: number) => setPageIndex(idx)}
        dotColor={styles.dotStyle.color}
        activeDotColor={styles.activeDotStyle.color}
        paginationStyle={styles.dotsContainer}
        dotStyle={styles.dotStyle}
        activeDotStyle={styles.activeDotStyle}
      >
        {renderFirstPage()}
        {renderSecondPage()}
        {renderForthPage()}
      </Swiper>
      <Animated.View pointerEvents="none" style={{ ...styles.iconContainer, opacity: iconOpacity }}>
        <Icon name="arrow-forward-outline" width={40} height={46} tintColor="#FFFFFF" />
      </Animated.View>
      <Animated.View
        pointerEvents={pageIndex === 2 ? "auto" : "none"}
        style={{ ...styles.buttonContainer, opacity: buttonOpacity }}
      >
        <Button
          status="control"
          size="giant"
          appearance="outline"
          textStyle={styles.buttonTextStyle}
          onPress={handleOnboardingDone}
        >
          {buttonText}
        </Button>
      </Animated.View>
    </SafeAreaLayout>
  );
};

export default Onboarding;

const styles = StyleSheet.create({
  content: {
    alignItems: "center",
    width: "90%"
  },
  page: {
    flex: 1,
    alignItems: "center"
  },
  textContainterPage1To3: {
    position: "absolute",
    bottom: "22.5%",
    paddingBottom: "4.2%"
  },
  textContainterPage4: {
    position: "absolute",
    bottom: "26%",
    paddingBottom: "6.2%"
  },
  headline: {
    letterSpacing: 3,
    textAlign: "center",
    marginBottom: "3.3%"
  },
  descriptionContainer: {
    width: "90%"
  },
  description: {
    textAlign: "center"
  },
  iconContainer: {
    position: "absolute",
    alignSelf: "center",
    bottom: "13%"
  },
  buttonContainer: {
    position: "absolute",
    alignSelf: "center",
    bottom: "15%",
    width: "80%"
  },
  buttonTextStyle: {
    letterSpacing: 2
  },
  dotsContainer: {
    marginBottom: "7.3%"
  },
  dotStyle: {
    color: "#FFFFFF",
    width: 15,
    height: 15,
    marginLeft: 7.5,
    marginRight: 7.5,
    opacity: 0.28,
    borderRadius: 7.5
  },
  activeDotStyle: {
    color: "#FFFFFF",
    width: 15,
    height: 15,
    marginLeft: 7.5,
    marginRight: 7.5,
    borderRadius: 7.5
  }
});
