import React from "react";

import useRenderCount from "src/hooks/useRenderCount";
import { Container } from "src/components/container/Container";

interface IProps {}
type HomeScreenProps = IProps;

const HomeScreenComponent: React.FC<HomeScreenProps> = () => {
  useRenderCount("HomeScreenComponent");

  return (
    <Container level="2">
      {/* <ScrollView contentContainerStyle={{ flex: 1, alignItems: "center" }}> */}

      {/* </ScrollView> */}
    </Container>
  );
};

export default HomeScreenComponent;
