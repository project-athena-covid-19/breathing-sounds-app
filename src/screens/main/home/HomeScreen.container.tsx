import React, { useLayoutEffect } from "react";
import _ from "lodash";

import { IHomeScreenContainerProps } from "src/navigation/navigators/main/HomeNavigator";
import HomeScreenComponent from "./HomeScreen.component";
import useRenderCount from "src/hooks/useRenderCount";
import { HeaderButtonWrapper, HeaderButtonItem } from "src/components";
import { StatusBar } from "src/components/statusBar/StatusBar";

interface IProps {}
type HomeScreenContainerProps = IProps & IHomeScreenContainerProps;
const HomeScreenContainer: React.FC<HomeScreenContainerProps> = (props) => {
  useRenderCount("HomeScreenContainer");

  useLayoutEffect(() => {
    props.navigation.setOptions({
      title: "Home"
    });
  }, [props.navigation]);

  return (
    <React.Fragment>
      <StatusBar barStyle="light-content" />
      <HomeScreenComponent />
    </React.Fragment>
  );
};

export default HomeScreenContainer;
