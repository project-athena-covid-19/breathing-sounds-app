import { useEffect, useRef } from "react";
export default function useRenderCount(compontentName: string) {
  const count = useRef(1);

  useEffect(() => {
    count.current += 1;
  });

  console.log("render - " + compontentName + " (" + count.current + ")");
  return {};
}
