import { useEffect } from "react";
import { Notifications } from "expo";
// import * as Segment from "@src/common/services/segment";
// import useLanguage from "./use-language";
import { Platform } from "react-native";

export default function useAppStart() {
  //   const { initLanguage } = useLanguage();

  useEffect(() => {
    async function init() {
      if (Platform.OS === "ios") {
        Notifications.setBadgeNumberAsync(0);
      }
      //   Segment.trackWithProperties(Segment.Events.APPLICATION_OPENED, {});
      //   await initLanguage();
    }
    init();
    // return () => {
    //   Segment.trackWithProperties(Segment.Events.APPLICATION_CLOSED, {});
    // };
  }, []);

  return {};
}
