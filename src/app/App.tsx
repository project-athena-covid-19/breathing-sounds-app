import React from "react";
import { StatusBar } from "src/components/statusBar/StatusBar";
import AppNavigator from "src/navigation/navigators/AppNavigator";
import useRenderCount from "src/hooks/useRenderCount";

interface IProps {}

export const AppContainer: React.FC<IProps> = () => {
  useRenderCount("AppContainer");
  return (
    <React.Fragment>
      <StatusBar />
      <AppNavigator />
    </React.Fragment>
  );
};
