import React from "react";
import { IconRegistry, ApplicationProvider } from "@ui-kitten/components";
import { EvaIconsPack } from "@ui-kitten/eva-icons";
import { AppearanceProvider } from "react-native-appearance";
import { SafeAreaProvider } from "react-native-safe-area-context";
import { Theming, Theme } from "src/services/theme.service";

import { appMappings, appThemes } from "src/theme/theme";
import LanguageService from "src/services/language.service";
import useRenderCount from "src/hooks/useRenderCount";
import AuthService from "src/services/auth.service";
import OnboardingService from "src/services/onboarding.service";

interface IProviderProps {
  themeKey: Theme;
  languageCode: string;
  authToken?: string;
  isOnboardingDone?: boolean;
}
export const Provider: React.FC<IProviderProps> = (props) => {
  useRenderCount("Provider");

  const [mappingContext, currentMapping] = Theming.useMapping(appMappings, "eva");
  const [themeContext, currentTheme] = Theming.useTheming(appThemes, "eva", props.themeKey);
  return (
    <React.Fragment>
      <IconRegistry icons={[EvaIconsPack]} />
      <AppearanceProvider>
        <ApplicationProvider {...currentMapping} theme={currentTheme}>
          <Theming.MappingContext.Provider value={mappingContext}>
            <Theming.ThemeContext.Provider value={themeContext}>
              <LanguageService.Provider activeLanguageCode={props.languageCode}>
                <AuthService.Provider cachedToken={props.authToken}>
                  <OnboardingService.Provider isOnboardingDone={props.isOnboardingDone}>
                    <SafeAreaProvider>{props.children}</SafeAreaProvider>
                  </OnboardingService.Provider>
                </AuthService.Provider>
              </LanguageService.Provider>
            </Theming.ThemeContext.Provider>
          </Theming.MappingContext.Provider>
        </ApplicationProvider>
      </AppearanceProvider>
    </React.Fragment>
  );
};
