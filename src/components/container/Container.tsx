import React from "react";
import { Layout, LayoutProps, styled } from "@ui-kitten/components";
import { useHeaderHeight } from "@react-navigation/stack";

interface IProps {
  isTransparentHeader?: boolean;
}
export type ContainerProps = IProps & LayoutProps;

const ContainerComponent: React.FC<ContainerProps> = ({ themedStyle, style, isTransparentHeader, ...props }) => {
  const headerHeight = useHeaderHeight();
  const paddingTop = isTransparentHeader ? headerHeight : 0;
  return (
    <Layout style={[themedStyle, style, { paddingTop }]} {...props}>
      {props.children}
    </Layout>
  );
};

// @ts-ignore
ContainerComponent.styledComponentName = "Container";

export const Container = styled(ContainerComponent);
