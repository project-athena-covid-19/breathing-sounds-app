import React, { forwardRef } from "react";
import { Input as KittenInput, InputProps as KittenInputProps, styled } from "@ui-kitten/components";

interface IProps {
  status?: "basic" | "primary" | "success" | "info" | "warning" | "danger" | "control";
  size?: "small" | "medium" | "large";
}
export type InputProps = IProps & KittenInputProps;

export const InputComponent: React.FC<InputProps> = forwardRef((props, ref) => {
  return (
    <KittenInput ref={ref} status="basic" {...props}>
      {props.children}
    </KittenInput>
  );
});

// @ts-ignore
InputComponent.styledComponentName = "Input";

export default styled(InputComponent);
