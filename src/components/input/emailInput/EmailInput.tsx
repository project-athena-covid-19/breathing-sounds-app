import React, { forwardRef } from "react";
import Icon from "src/components/icon/Icon";
import Input, { InputProps } from "src/components/input/Input";

interface IProps {}

type EmailInputProps = IProps & InputProps;

const EmailIcon = (style: any) => <Icon {...style} name={"person"} />;

const EmailInput: React.FC<EmailInputProps> = forwardRef((props, ref) => {
  return (
    <Input
      ref={ref}
      placeholder="E-Mail Adresse"
      autoCapitalize="none"
      icon={EmailIcon}
      keyboardType="email-address"
      {...props}
    />
  );
});

export default EmailInput;
EmailInput.displayName = "EmailInput";
