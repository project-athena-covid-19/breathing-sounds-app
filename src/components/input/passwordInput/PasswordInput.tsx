import React, { forwardRef } from "react";
import Icon from "src/components/icon/Icon";
import Input, { InputProps } from "src/components/input/Input";
import { TouchableOpacity } from "react-native";

interface IProps {}

type PasswordInputProps = IProps & InputProps;

const PasswordInput: React.FC<PasswordInputProps> = forwardRef((props, ref) => {
  const [secureTextEntry, setSecureTextEntry] = React.useState(true);
  const onIconPress = () => {
    setSecureTextEntry(!secureTextEntry);
  };
  const renderIcon = (style: any) => (
    <TouchableOpacity onPress={onIconPress}>
      <Icon {...style} name={secureTextEntry ? "eye-off" : "eye"} />
    </TouchableOpacity>
  );
  return (
    <Input ref={ref} placeholder="Passwort" secureTextEntry={secureTextEntry} icon={renderIcon} {...props}></Input>
  );
});

export default PasswordInput;

PasswordInput.displayName = "PasswordInput";
