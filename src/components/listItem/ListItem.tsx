import React from "react";
import { ListItem as KittenListItem, ListItemProps as KittenListItemProps, styled } from "@ui-kitten/components";

interface IProps {}
export type ListItemProps = IProps & KittenListItemProps;

export const ListItemComponent: React.FC<ListItemProps> = (props) => {
  return <KittenListItem {...props}>{props.children}</KittenListItem>;
};

// @ts-ignore
ListItemComponent.styledComponentName = "ListItem";

export default styled(ListItemComponent);
