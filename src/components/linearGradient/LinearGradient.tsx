import React from "react";
import { LinearGradient as ExpoLinearGradient } from "expo-linear-gradient";
import { StyleProp, ViewStyle } from "react-native";

export interface ILinearGradientProps {
  children?: any;
  style?: StyleProp<ViewStyle>;
}

export default function LinearGradient(props: ILinearGradientProps) {
  return (
    <ExpoLinearGradient colors={["#F6EC3D", "#408EE3"]} start={[0, 0]} end={[1, 1]} style={[{ flex: 1 }, props.style]}>
      {props.children}
    </ExpoLinearGradient>
  );
}
