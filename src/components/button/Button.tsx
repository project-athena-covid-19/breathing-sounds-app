import React from "react";
import { styled, ButtonProps as KittenButtonProps, Button as KittenButton } from "@ui-kitten/components";
import { ActivityIndicator } from "react-native";

interface IProps {
  appearance?: "filled" | "outline" | "ghost";
  status?: "basic" | "primary" | "success" | "info" | "warning" | "danger" | "control";
  size?: "tiny" | "small" | "medium" | "large" | "giant";
  loading?: boolean;
  children?: string;
}
export type ButtonProps = IProps & KittenButtonProps;

const renderChildren = (props: ButtonProps): string | undefined => {
  if (props.loading) {
    return undefined;
  }

  if (props.children) {
    return props.children;
  }

  return undefined;
};

const LoadingIcon = () => <ActivityIndicator />;

const ButtonComponent: React.FC<ButtonProps> = (props) => {
  const { loading } = props;

  const loadingIconProps = loading
    ? {
        icon: LoadingIcon,
        disabled: true
      }
    : {};

  return (
    <KittenButton {...props} {...loadingIconProps}>
      {renderChildren(props)}
    </KittenButton>
  );
};

// @ts-ignore
ButtonComponent.styledComponentName = "Button";

export default styled(ButtonComponent);
