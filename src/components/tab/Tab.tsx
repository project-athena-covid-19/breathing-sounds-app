import React from "react";
import { Tab as KittenTab, TabProps as KittenTabProps, styled } from "@ui-kitten/components";

interface IProps {}
export type TabProps = IProps & KittenTabProps;

export const TabComponent: React.FC<TabProps> = (props) => {
  return <KittenTab {...props}>{props.children}</KittenTab>;
};

// @ts-ignore
TabComponent.styledComponentName = "Tab";

export default styled(TabComponent);
