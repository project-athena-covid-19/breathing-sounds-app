import React from "react";
import { StatusBar as RNStatusBar, Platform } from "react-native";
import { ThemedComponentProps, styled } from "@ui-kitten/components";

interface IProps {
  hidden?: boolean;
  barStyle?: "dark-content" | "light-content";
}
type StatusBarProps = IProps & ThemedComponentProps;

const StatusBarComponent: React.FC<StatusBarProps> = ({ themedStyle, hidden, ...statusBarProps }) => {
  if (hidden) {
    return <RNStatusBar hidden />;
  }

  if (Platform.OS === "android") {
    return null;
  }

  return <RNStatusBar {...themedStyle} {...statusBarProps} />;
};

//@ts-ignore
StatusBarComponent.styledComponentName = "StatusBar";

export const StatusBar = styled(StatusBarComponent);
