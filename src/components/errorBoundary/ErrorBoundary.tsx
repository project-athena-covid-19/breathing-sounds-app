import React from "react";

// import * as Sentry from "@src/common/services/sentry";

interface IProps {}
interface IState {}
class ErrorBoundary extends React.Component<IProps, IState> {
  public componentDidCatch = (error: Error, errorInfo: object) => {
    // Sentry.captureException(error, {
    //   extra: JSON.stringify(errorInfo),
    //   logger: Sentry.Events.LOGGER_ERROR_BOUNDARY,
    //   level: "info"
    // });
  };

  public render() {
    const { children } = this.props;
    return children;
  }
}

export default ErrorBoundary;
