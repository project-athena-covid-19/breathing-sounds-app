import React, { useState, useCallback } from "react";
import { useHeaderHeight } from "@react-navigation/stack";
import { StatusBar } from "src/components/statusBar/StatusBar";
import LinearGradient from "src/components/linearGradient/LinearGradient";
import { View, StyleSheet, Dimensions } from "react-native";
import ScrollView from "src/components/scrollView/ScrollView";
import Text from "src/components/text/Text";

const SCREEN_HEIGHT = Math.round(Dimensions.get("window").height);

interface LinearGradientHeaderScrollViewProps {
  title?: string;
}

const LinearGradientHeaderScrollView: React.FC<LinearGradientHeaderScrollViewProps> = (props) => {
  const headerHeight = useHeaderHeight();
  const [firstChildDimensions, setFirstChildDimensions] = useState({ height: 0, width: 0, initalRender: false });

  const onLayout = useCallback((event) => {
    if (firstChildDimensions.initalRender) return undefined;
    let { width, height } = event.nativeEvent.layout;
    setFirstChildDimensions({ initalRender: true, width, height });
  }, []);

  return (
    <React.Fragment>
      <StatusBar barStyle="light-content" />
      <LinearGradient style={styles.linearGradient}></LinearGradient>
      <View style={{ height: headerHeight }}></View>
      <ScrollView
        style={[styles.scrollView, { paddingTop: headerHeight }]}
        contentContainerStyle={styles.scrollViewContainerContent}
      >
        {props.title ? (
          <Text appearance="default" status="control" category="h1" style={styles.headerText}>
            {props.title}
          </Text>
        ) : null}

        <View
          style={[
            styles.headerSpacing,
            {
              height: firstChildDimensions.height / 3,
              top: firstChildDimensions.height - (props.title ? 0 : 60)
            }
          ]}
        />
        {React.Children.map(props.children, (child, index) => {
          if (index === 0) {
            return React.cloneElement(child, { onLayout });
          }
          return child;
        })}
      </ScrollView>
    </React.Fragment>
  );
};
export default LinearGradientHeaderScrollView;

const styles = StyleSheet.create({
  headerSpacing: {
    width: "100%",
    backgroundColor: "white",
    position: "absolute",
    left: 0,
    right: 0
  },
  linearGradient: {
    height: SCREEN_HEIGHT / 2,
    flex: 0,
    position: "absolute",
    top: 0,
    left: 0,
    right: 0
  },
  scrollView: {
    flex: 1
  },
  scrollViewContainerContent: {
    width: "100%",
    justifyContent: "center",
    alignItems: "center"
  },
  headerText: {
    marginBottom: 20
  }
});
