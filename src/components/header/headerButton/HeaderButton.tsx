import React from "react";
import {
  HeaderButton as ReactNavigationHeaderButton,
  HeaderButtonProps as ReactNavigationHeaderButtonProps
} from "react-navigation-header-buttons";
import Icon from "src/components/icon/Icon";
import { ViewStyle } from "react-native";

interface IIconComponentProps {
  name?: string;
  key?: string;
  color?: string;
  iconStyle?: ViewStyle;
  style?: any;
}
const IconComponent: React.FC<IIconComponentProps> = ({ name, key, color, iconStyle, style, ...props }) => {
  if (!name) return null;
  return <Icon key={key} name={name} tintColor={color} style={[...style, iconStyle]} {...props} />;
};

interface IProps {
  tintColor?: string;
  iconStyle?: ViewStyle;
}
type HeaderButtonProps = IProps & ReactNavigationHeaderButtonProps;

const HeaderButton: React.FC<HeaderButtonProps> = ({ tintColor, iconStyle, ...props }) => {
  return (
    <ReactNavigationHeaderButton
      {...props}
      color={tintColor}
      IconComponent={(iconComponentProp: any) => (
        <IconComponent {...iconComponentProp} iconStyle={iconStyle}></IconComponent>
      )}
    />
  );
};

export default HeaderButton;
