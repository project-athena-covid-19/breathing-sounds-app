import React from "react";
import { HeaderButtons, Item } from "react-navigation-header-buttons";
import HeaderButton from "../headerButton/HeaderButton";

interface HeaderButtonWrapperProps {}

export const HeaderButtonWrapper: React.FC<HeaderButtonWrapperProps> = ({ children, ...props }) => {
  return (
    <HeaderButtons HeaderButtonComponent={HeaderButton} {...props}>
      {children}
    </HeaderButtons>
  );
};

export const HeaderButtonItem = Item;
