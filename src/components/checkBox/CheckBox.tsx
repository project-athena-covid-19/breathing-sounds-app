import React from "react";
import { CheckBox as KittenCheckBox, CheckBoxProps as KittenCheckBoxProps, styled } from "@ui-kitten/components";

interface IProps {
  status?: "basic" | "primary" | "success" | "info" | "warning" | "danger" | "control";
}
export type TextProps = IProps & KittenCheckBoxProps;

export const CheckBoxComponent: React.FC<TextProps> = (props) => {
  return <KittenCheckBox {...props}>{props.children}</KittenCheckBox>;
};

// @ts-ignore
CheckBoxComponent.styledComponentName = "CheckBox";

export default styled(CheckBoxComponent);
