import React from "react";
import {
  RadioGroup as KittenRadioGroup,
  RadioGroupProps as KittenRadioGroupProps,
  styled
} from "@ui-kitten/components";

interface IProps {}
export type RadioGroupProps = IProps & KittenRadioGroupProps;

export const RadioGroupComponent: React.FC<RadioGroupProps> = (props) => {
  return <KittenRadioGroup {...props}>{props.children}</KittenRadioGroup>;
};

// @ts-ignore
RadioGroupComponent.styledComponentName = "RadioGroup";

export default styled(RadioGroupComponent);
