import React from "react";
import { Text as KittenText, TextProps as KittenTextProps, styled } from "@ui-kitten/components";

interface IProps {
  status?: `basic` | `primary` | `success` | `info` | `warning` | `danger` | `control`;
  appearance?: `default` | `alternative` | `hint`;
  category?: `h1` | `h2` | `h3` | `h4` | `h5` | `h6` | `s1` | `s2` | `p1` | `p2` | `c1` | `c2` | `label` | `p1`;
}
export type TextProps = IProps & KittenTextProps;

export const TextComponent: React.FC<TextProps> = (props) => {
  return <KittenText {...props}>{props.children}</KittenText>;
};

// @ts-ignore
TextComponent.styledComponentName = "Text";

export default styled(TextComponent);
