import React from "react";
import { styled, StyledComponentProps, Icon as EvaIcon } from "@ui-kitten/components";
// import { ImageStyle } from "react-native";

interface IProps {
  name?: string;
  packName?: "swipe" | "nav-1-home" | "nav-2-profile" | "nav-3-scan" | "nav-4-star" | "nav-5-menu" | "turn" | "close";
  pack?: "assets";
  tintColor?: string;
  height?: number;
  width?: number;
  style?: any;
}
export type IconProps = IProps & StyledComponentProps;

const IconComponent: React.FC<IconProps> = ({ themedStyle, style, name, packName, pack, tintColor, height, width }) => {
  if (pack) {
    return (
      <EvaIcon
        name={packName}
        pack={pack}
        style={style}
        tintColor={tintColor || themedStyle!.tintColor}
        height={height || themedStyle!.height}
        width={width || themedStyle!.width}
      />
    );
  }
  return (
    <EvaIcon
      name={name}
      style={[themedStyle, style]}
      fill={tintColor || themedStyle!.tintColor}
      height={height || themedStyle!.height}
      width={width || themedStyle!.width}
    />
  );
};

// @ts-ignore
IconComponent.styledComponentName = "Icon";

export default styled(IconComponent);
