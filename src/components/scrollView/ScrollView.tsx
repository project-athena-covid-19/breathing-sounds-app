import React from "react";
import { ScrollView as RNScrollView, ScrollViewProps as RNScrollViewProps } from "react-native";

interface IProps {}
export type ScrollViewProps = IProps & RNScrollViewProps;

const ScrollView: React.FC<ScrollViewProps> = ({ ...props }) => {
  return <RNScrollView {...props}>{props.children}</RNScrollView>;
};

export default ScrollView;
