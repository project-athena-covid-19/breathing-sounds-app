import React, { forwardRef } from "react";
import { Toggle as KittenToggle, ToggleProps as KittenToggleProps, styled } from "@ui-kitten/components";
import { View, ActivityIndicator, StyleSheet } from "react-native";

interface IProps {
  loading?: boolean;
  status?: "basic" | "primary" | "success" | "info" | "warning" | "danger" | "control";
}
export type ToggleProps = IProps & KittenToggleProps;

export const ToggleComponent: React.FC<ToggleProps> = (props) => {
  return (
    <View style={styles.container}>
      {!!props.loading ? <ActivityIndicator></ActivityIndicator> : null}
      <KittenToggle {...props}>{props.children}</KittenToggle>
    </View>
  );
};

// @ts-ignore
ToggleComponent.styledComponentName = "Toggle";

export default styled(ToggleComponent);

const styles = StyleSheet.create({
  container: {
    flexDirection: "row"
  }
});
