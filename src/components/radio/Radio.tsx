import React from "react";
import { Radio as KittenRadio, RadioProps as KittenRadioProps, styled } from "@ui-kitten/components";

interface IProps {
  status?: "basic" | "primary" | "success" | "info" | "warning" | "danger" | "control";
}
export type TextProps = IProps & KittenRadioProps;

export const RadioComponent: React.FC<TextProps> = (props) => {
  return <KittenRadio {...props}>{props.children}</KittenRadio>;
};

// @ts-ignore
RadioComponent.styledComponentName = "Radio";

export default styled(RadioComponent);
