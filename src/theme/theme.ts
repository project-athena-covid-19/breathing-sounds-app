import * as EvaCustom from "src/theme/eva/index";
import * as eva from "@eva-design/eva";

export const appMappings = {
  eva: {
    mapping: eva.mapping,
    customMapping: EvaCustom.mapping
  }
};

export const appThemes = {
  eva: {
    light: { ...eva.light, ...EvaCustom.light, ...EvaCustom.customTheme },
    dark: { ...eva.dark, ...EvaCustom.dark, ...EvaCustom.customTheme }
  }
};
