import React from "react";

import { AppLoading, LoadFontsTask, LoadAssetsTask, Task } from "src/components/appLoading/AppLoading";
import ErrorBoundary from "src/components/errorBoundary/ErrorBoundary";
import { SplashImage } from "src/components/splashImage/SplashImage";
import { AppContainer } from "src/app/App";
import { Provider } from "src/app/Provider";
import { CacheService } from "src/services/cache.service";
import useAppStart from "src/hooks/useAppStart";
import { YellowBoxService } from "src/services/yellowBox.service";
import { Theme } from "src/services/theme.service";
import "src/services/i18n.service";

interface IAppNewProps {}

const defaultConfig: { themeKey: Theme; languageCode: string } = {
  themeKey: "light",
  languageCode: "en"
};
const loadingTasks: Array<Task> = [
  // Should be used it when running Expo.
  // In Bare RN Project this is configured by react-native.config.js
  () =>
    LoadFontsTask({
      "opensans-regular": require("./assets/fonts/opensans-regular.ttf"),
      "roboto-regular": require("./assets/fonts/roboto-regular.ttf"),
      "raleway-light": require("./assets/fonts/raleway/Raleway-Light.ttf"),
      "raleway-regular": require("./assets/fonts/raleway/Raleway-Regular.ttf"),
      "raleway-bold": require("./assets/fonts/raleway/Raleway-Bold.ttf"),
      "raleway-semi-bold": require("./assets/fonts/raleway/Raleway-SemiBold.ttf")
    }),
  () =>
    LoadAssetsTask([
      // require("./assets/images/avatar.png")
      // require("./assets/images/notification.json"),
      // require("./assets/images/notification_dark_theme.json")
    ]),
  () => CacheService.getThemeKey(defaultConfig.themeKey).then((result) => ["themeKey", result]),
  () => CacheService.getLanguageCode(defaultConfig.languageCode).then((result) => ["languageCode", result]),
  () => CacheService.getAuthToken().then((result) => ["authToken", result]),
  () => CacheService.getOnboardingState().then((result) => ["isOnboardingDone", result])
];

const App: React.FC<IAppNewProps> = () => {
  YellowBoxService.removeYellowBoxes();
  useAppStart();

  const onBoardingDone = false;
  CacheService.setOnboardingDone(onBoardingDone); // For testing only!!!

  return (
    <ErrorBoundary>
      <AppLoading tasks={loadingTasks} initialConfig={defaultConfig} placeholder={Splash}>
        {(props) => (
          <Provider {...props}>
            <AppContainer />
          </Provider>
        )}
      </AppLoading>
    </ErrorBoundary>
  );
};

const Splash: React.FC<{ loading: boolean }> = ({ loading }) => (
  <SplashImage loading={loading} source={require("./assets/images/splash.png")} />
);

export default App;
