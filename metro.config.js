// eslint-disable-next-line @typescript-eslint/no-var-requires
const MetroConfig = require("@ui-kitten/metro-config");
const { getDefaultConfig } = require("metro-config");

const evaConfig = {
  evaPackage: "@eva-design/eva",
  // Optional, but may be useful when using mapping customization feature.
  customMappingPath: "./src/theme/eva/mapping.json"
};

module.exports = (async () => {
  const {
    resolver: { sourceExts, assetExts }
  } = await getDefaultConfig();

  return MetroConfig.create(evaConfig, {
    transformer: {
      babelTransformerPath: require.resolve("react-native-svg-transformer")
    },
    resolver: {
      assetExts: assetExts.filter((ext) => ext !== "svg"),
      sourceExts: [...sourceExts, "svg"]
    }
  });
})();
