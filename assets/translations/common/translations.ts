export const common = {
  en: {
    placeholder: {
      phoneNumber: "Phone Number",
      password: "Password",
      firstName: "First Name",
      lastName: "Last Name",
      email: "Email"
    },
    label: {
      password: "Password",
      firstName: "First Name",
      lastName: "Last Name",
      email: "Email",
      Today: "Today",
      Yesterday: "Yesterday",
      This_Week: "This Week",
      Last_Week: "Last Week",
      This_Month: "This Month",
      Last_Month: "Last Month",
      This_Year: "This Year",
      Last_Year: "Last Year",
      All_Time: "All Time"
    },
    action: {
      edit: "Edit",
      close: "Close",
      back: "Back",
      ok: "OK",
      cancel: "Cancel",
      delete: "Delete",
      save: "Save",
      add: "Add",
      next: "Next",
      tryAgain: "Try again",
      readMore: "Read more",
      showLess: "Show less",
      logout: "Logout",
      skip: "Skip",
      finish: "Finish",
      clearFilters: "Clear Filters"
    }
  },
  de: {
    placeholder: {
      phoneNumber: "Telefonnummer",
      password: "Passwort",
      firstName: "Vorname",
      lastName: "Nachname",
      email: "E-Mail"
    },
    label: {
      phoneNumber: "Telefonnummer",
      password: "Passwort",
      firstName: "Vorname",
      lastName: "Nachname",
      email: "E-Mail",
      Today: "Heute",
      Yesterday: "Gestern",
      This_Week: "Diese Woche",
      Last_Week: "Letzte Woche",
      This_Month: "Dieser Monat",
      Last_Month: "Letzter Monat",
      This_Year: "Dieses Jahr",
      Last_Year: "Letztes Jahr",
      All_Time: "Seit Beginn"
    },
    action: {
      edit: "Bearbeiten",
      close: "Schließen",
      back: "Zurück",
      ok: "OK",
      cancel: "Abbrechen",
      delete: "Löschen",
      save: "Speichern",
      add: "Hinzufügen",
      next: "Weiter",
      tryAgain: "Noch einmal",
      readMore: "Mehr",
      showLess: "Einklappen",
      logout: "Abmelden",
      skip: "Überspringen",
      finish: "Fertig",
      clearFilters: "Filter löschen"
    }
  }
};
