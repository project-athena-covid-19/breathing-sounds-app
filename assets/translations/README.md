## Translation Guide

### 1. Key-Words

- heading - Translations related to headings in the document
- label - Translations which are used to identify parts of the document. e.g. form labels, table headers, etc.
- flash - For flash messages
- phrase - For small chunks of arbitrary text. e.g. "One short sentence"
- link - Translations related to followable links
- text - For large blocks of text, typically spanning multiple lines
- action - Translation related to actions in buttons etc..
- placeholder - Translation related to placeholders in inputs, text...
