export const error = {
  en: {
    permission: {
      heading: {
        cameraRollPermission: "Photos",
        cameraPermission: "Camera",
        alertPermissionChange: "Permission required"
      },
      phrase: {
        changePermissionsRequest: "Please change this permission in the app settings"
      }
    },
    default: {
      heading: "Oops!",
      text: "Sorry. Seems like there was an error. Please try again."
    },
    heading: {
      noConnection: "No internet connection",
      loginFailed: "Login failed"
    },
    phrase: {
      noConnection: "You need an active internet connection.",
      unableToConnectToFacebook: "Unable to connect to facebook."
    }
  },
  de: {
    permission: {
      heading: {
        cameraRollPermission: "Fotos",
        cameraPermission: "Kamera",
        alertPermissionChange: "Erlaubnis erteilen"
      },
      phrase: {
        changePermissionsRequest: "Bitte gehe zu deinen App Einstellungen und ändere diese"
      }
    },
    default: {
      heading: "Oops!",
      text: "Entschulige. Sieht so aus, als wäre ein Fehler aufgetreten. Bitte versuche es noch einmal."
    },
    heading: {
      noConnection: "Keine Internet Verbindung",
      loginFailed: "Log-in fehlgeschlagen"
    },
    phrase: {
      noConnection: "Du brauchst eine aktive Internetverbindung.",
      unableToConnectToFacebook: "Verbindung zu Facebook fehlgeschlagen."
    }
  }
};
